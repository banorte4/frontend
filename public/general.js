
function numberChange(el) {
    el.value = el.value.replace(/[^0-9]/g, '');
}

let onlyNumber = function (el) {
    let valor = el.value;
    let nuevoValor = "";
    for (const element of valor) {
        if (element >= '0' && element <= '9') {
            nuevoValor += element;
        }
    }
    el.value = nuevoValor;
}

function actualizarRowsSelect() {
    let svgs = $(".caret")
    svgs.each(function (n, svg) {
        svg.children[0].remove();
        svg.lastElementChild.setAttribute('fill-rule', 'nonzero');
        svg.lastElementChild.setAttribute('fill', '#5b6670');
        svg.lastElementChild.setAttribute('d', 'M 15.372340425531915 1.627659574468085  L 8.287234042553191 9  L 0.6276595744680862 1.6276595744680853  C 0.24468085106383095 1.24468085106383  0.24468085106383095 0.6702127659574472  0.6276595744680862 0.28723404255319196  C 1.0106382978723416 -0.09574468085106337  1.5851063829787244 -0.09574468085106337  1.9680851063829796 0.28723404255319196  L 8.287234042553191 6.319148936170212  L 14.03191489361702 0.2872340425531915  C 14.414893617021278 -0.09574468085106377  14.98936170212766 -0.09574468085106377  15.372340425531915 0.2872340425531915  C 15.755319148936172 0.6702127659574468  15.755319148936172 1.2446808510638299  15.372340425531915 1.627659574468085  Z ')
    });
};
