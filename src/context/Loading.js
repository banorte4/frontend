
export const reducer = (state, action) => {
    switch (action.type) {
        case "show":
            return {
                active: true
            }

        default:
            return {
                active: false
            }
    }
}

export const initialState = {
    active: false
}