import React, { useState, createContext, useContext, useEffect } from "react";
import { LoadingContext , UrlServicios } from './ContextLoading';

export const Login = (props) => {
    const [password, setPassword] = React.useState('');
    const formRef = React.useRef();

    const { loading, setLoading } = useContext(LoadingContext);

    function handleSubmit(evt) {
        setLoading(true);

        evt.preventDefault();
        const formData = new FormData(formRef.current);
        const data = Object.fromEntries(formData);
        const controller = new AbortController();
        setTimeout(() => controller.abort(), 10000);
        const url = UrlServicios('users/login/');
        
        fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
        .then((response) => response.json())
        .then((actualData) => {
            if (actualData.token) {
                sessionStorage.setItem("access_token", actualData.token);
                sessionStorage.setItem("fullname", actualData.fullname);
                sessionStorage.setItem("rol", actualData.rol.id)
                props.login(actualData.token);
            }
            else {
                window.M.toast({ html: 'Servicio se consulto, pero no genero el token de acceso!' })
            }
        })
        .catch((err) => {
            setPassword("");
            setLoading(false);
            window.M.toast({ html: 'Credenciales invalidas!' });
        });

    }
    return (
        <div className="modal login" id="login" style={{ 'width': '520px' }} >
            <div className="icon-close modal-close">
                <img src="../img/icono_cerrar.svg" alt="icon" />
            </div>
            <div className="center" style={{ padding: '10px' }}>                
                <h2> Usuario / Contraseña </h2>
                <form onSubmit={handleSubmit} ref={formRef}>
                    <div className="row " >
                        <div className="input-field s12">
                            <input minLength="1" maxLength="8" type="text" title="user" id="user"
                                pattern="a[0-9]{1,7}" size="10"
                                name="username"
                                className="validate" />
                            <label htmlFor='user'>Usuario de Red </label>
                            <span className="helper-text" data-error="Ejemplo del usuario: a8049951" data-success=""></span>
                        </div>
                    </div>
                    <div className="row " >
                        <div className="input-field s12">
                            <input minLength="1" type="password" title="password"
                                autoComplete="off"
                                name="password"
                                className="validate"
                                defaultValue={password}
                                onChange={(event) => {
                                    setPassword(event.target.value);
                                }}
                            />
                            <label htmlFor={'pass'}>Password</label>
                            <span className="helper-text" data-error="Completar campo" data-success=""></span>
                        </div>
                    </div>
                    <br></br>
                    <button type="submit" className="btn" >Acceder</button>
                </form>
            </div>
        </div>
    )
}
