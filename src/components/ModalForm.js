import React, { useEffect, useMemo, useState } from 'react';
import { Player } from '@lottiefiles/react-lottie-player';

import info from './animations/LottieAviso.json'
import error from './animations/LottieAlertaError.json'
import success from './animations/LottieCheckExito.json'
import warning from './animations/LottieConfirmacion.json'

import bloqueado from './animations/LottieAumentodeSeguridad.json'

export const ModalForm = ({ onOpen, idModal, icon, title, description, textButton, onClose, onSubmit }) => {
    /**
     * Funciones que se ejecutan al dar clic al botón principal
     */
    const handleSubmit = () => {
        onSubmit();
        onClose();
        window.jQuery('#' + idModal).modal('close');
    };
    /**
     * Iniicio del componente
     */
    useEffect(() => {
        if( typeof onOpen === "function")
        {
            setTimeout(() => {
                onOpen();
                window.jQuery('#' + idModal).modal('open');
            }, 1000);
        }
    }, [onOpen]);
    /**
     * Regresa el json, requerido.
     * @returns 
     */
    const iconJson = () => {
        switch (icon) {
            case "error":
                return error;
            case "success":
                return success;
            case "warning":
                return warning;
            case "info":
                return info;
            default:
                return bloqueado;
        }
    };
    return (
        <div className="modal" modal-title={title} id={idModal} >
            <div className="icon-close modal-close">
                <img src="../img/icono_cerrar.svg" alt="icon" />
            </div>
            <div className="center" style={{ 'minHeight': '120px', padding: '5px' }}>
                <Player autoplay loop src={iconJson()} style={{ height: '80px', width: '80px' }}></Player>
                <h3>{title}</h3>
                <p>{description}</p>
                <div className="center" style={{ "width": " 320px", "margin": " 0px auto", "display": "block" }} >
                    <button type="submit" style={{ 'width': '120px', margin: '0px 5px' }} className="btn" onClick={handleSubmit} >{textButton}</button>
                </div>
            </div>
        </div>
    )
}
