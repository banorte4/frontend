import React, { useContext } from 'react'
import { LoadingContext } from './ContextLoading';
import './Loading.scss';

export const Loading = () => {

    const {loading, setLoading} = useContext(LoadingContext);
    if(!loading){
        return ;
    }
    return (
        <div id="loading" >
            <div id="loading-content">
                <div className="sk-folding-cube" hidden>
                    <div className="sk-cube1 sk-cube"></div>
                    <div className="sk-cube2 sk-cube"></div>
                    <div className="sk-cube4 sk-cube"></div>
                    <div className="sk-cube3 sk-cube"></div>
                </div>
                    <div className="cube" >
                        <div className="sides">
                            <div className="top"></div>
                            <div className="right"></div>
                            <div className="bottom"></div>
                            <div className="left"></div>
                            <div className="front"></div>
                            <div className="back"></div>
                        </div>
                    </div>
                <h3>DEVSECOPS</h3>
            </div>
            <div className='mensajeLoading'>
                Su petición esta en proceso <br/>Por favor, espere...
                <br></br>
                <br></br>
                
                <img src="../../img/banorte_logo_white.png" alt="logologin"></img>
            </div>
        </div>
        
    )
}
