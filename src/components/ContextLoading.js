
import { createContext } from 'react';

let getUrlBackend = function(path){
    if(window.location.origin.indexOf("localhost")>=0){
        let uri="http://localhost:8080/";
        return new URL(path, uri);
    }else{
        return new URL("api/"+path, window.location.origin);
    }
    
}
export const UrlServicios = getUrlBackend;
export const LoadingContext = createContext(null);