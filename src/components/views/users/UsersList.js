import React, { useEffect, useMemo, useState } from 'react';
import MaterialReactTable from 'material-react-table';
import { Modal } from '../../Modal';
import { LoadingContext, UrlServicios } from '../../ContextLoading';

//Para acciones del grid
import {
    Box,
    IconButton,
    Tooltip,
} from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';

export const UsersList = () => {
    //data and fetching state
    const [data, setData] = useState([]);
    const [isError, setIsError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isRefetching, setIsRefetching] = useState(false);
    const [rowCount, setRowCount] = useState(0);

    //table state
    const [columnFilters, setColumnFilters] = useState([]);
    const [globalFilter, setGlobalFilter] = useState('');
    const [sorting, setSorting] = useState([]);
    const [pagination, setPagination] = useState({
        pageIndex: 0,
        pageSize: 10,
    });

    const fetchData = async () => {
        if (typeof data != "undefined" && data.length) {
            setIsLoading(true);
        } else {
            setIsRefetching(true);
        }

        const url = UrlServicios('users/');
        url.searchParams.set(
            'start',
            `${pagination.pageIndex * pagination.pageSize}`,
        );
        url.searchParams.set('size', `${pagination.pageSize}`);
        url.searchParams.set('filters', JSON.stringify(columnFilters ?? []));
        url.searchParams.set('globalFilter', globalFilter ?? '');
        url.searchParams.set('sorting', JSON.stringify(sorting ?? []));
        try {
            const response = await fetch(url.href, { method: 'GET' }, { headers: { 'X-RapidAPI-Key': sessionStorage.getItem('access_token') } });
            const json = await response.json();
            if (json) {
                setData(json);
                setRowCount(json.length);
            }
        } catch (error) {
            setIsError(true);
            console.error(error);
            return;
        }
        setIsError(false);
        setIsLoading(false);
        setIsRefetching(false);
    };
    useEffect(() => {
        fetchData();
    }, [
        columnFilters,
        globalFilter,
        pagination.pageIndex,
        pagination.pageSize,
        sorting
    ]);
    const columns = useMemo(
        () => [
            {
                accessorKey: 'username', //access nested data with dot notation
                header: 'Usuario',
            },
            {
                accessorKey: 'fullname',
                header: 'Nombre',
            },
            {
                accessorKey: 'email', //normal accessorKey
                header: 'Correo',
            },
            {
                accessorKey: 'rol.descripcion', //normal accessorKey
                header: 'Rol',
            },
            {
                accessorKey: 'fechaactualizacion',
                header: 'Fecha Registro',
                accessorFn: (row) => new Date(row.fechaactualizacion), //convert to Date for sorting and filtering
                id: 'startDate',
                filterFn: 'lessThanOrEqualTo',
                sortingFn: 'datetime',
                Cell: ({ cell }) =>
                    cell.getValue()?.toISOString()
                , //render Date as a string
                Header: ({ column }) => <em>{column.columnDef.header}</em>, //custom header markup
            },
        ],
        []);

    return (
        <div className="">
            <MaterialReactTable
                columns={columns}
                data={data}
                // enableRowSelection
                getRowId={(row) => row.id}
                initialState={{ showColumnFilters: false }}
                manualFiltering
                manualPagination
                manualSorting
                editingMode="modal" //default
                enableColumnOrdering
                enableEditing
                // onEditingRowSave={handleSaveRowEdits}
                // onEditingRowCancel={handleCancelRowEdits}
                renderRowActions={({ row, table }) => (
                    <Box sx={{ display: 'flex', gap: '1rem' }}>
                        <Tooltip arrow placement="right" title="Editar">
                            <IconButton onClick={() => console.log(row)}>
                                <Edit />
                            </IconButton>
                        </Tooltip>
                        <Tooltip arrow placement="right" title="Borrar">
                            <IconButton color="error" onClick={() =>
                                window.jQuery('#modal-' + row.id).modal('open')

                            }>
                                <Delete />
                            </IconButton>
                        </Tooltip>
                        <Modal
                            idModal={row.original.id}
                            icon="error"
                            title={"Eliminar App " + row.original.appid}
                            description={"¿Quieres eliminar registro de  " + row.original.areasolicitante + "?"}
                            onOpen=""
                            onClose={
                                () => {
                                    const url = UrlServicios('users/delete/' + row.original.id);
                                    fetch(url, {
                                        method: 'POST',
                                        headers: { 'Content-Type': 'application/json' },
                                    }).then((response) => response.json())
                                        .then(() => {

                                        })
                                        .catch((err) => {
                                            console.log(err.message);
                                        });
                                    console.log("Eliminar Folio");
                                }
                            }
                            onSubmit={() => console.log("submit")}
                            textButton="Aceptar"
                        ></Modal>
                    </Box>
                )}
                onColumnFiltersChange={setColumnFilters}
                onGlobalFilterChange={setGlobalFilter}
                onPaginationChange={setPagination}
                onSortingChange={setSorting}
                rowCount={rowCount}
                state={{
                    columnFilters,
                    globalFilter,
                    isLoading,
                    pagination,
                    showAlertBanner: isError,
                    showProgressBars: isRefetching,
                    sorting,
                }}
            ></MaterialReactTable>
        </div>
    )
}
