import React, { useEffect, useMemo, useState } from 'react';
import MaterialReactTable from 'material-react-table';
import { Modal } from '../../Modal';
import { LoadingContext, UrlServicios } from '../../ContextLoading';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { ExportToCsv } from 'export-to-csv';

import FileDownloadIcon from '@mui/icons-material/FileDownload';

import {
    Box,
    Button,
    IconButton,
    Tooltip,
} from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';

export const AppList = () => {
    //data and fetching state
    const [data, setData] = useState([]);
    const [isError, setIsError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isRefetching, setIsRefetching] = useState(false);
    const [rowCount, setRowCount] = useState(0);

    //table state
    const [columnFilters, setColumnFilters] = useState([]);
    const [globalFilter, setGlobalFilter] = useState('');
    const [sorting, setSorting] = useState([]);
    const [pagination, setPagination] = useState({
        pageIndex: 0,
        pageSize: 10,
    });

    const fetchData = async () => {
        if (typeof data != "undefined" && data.length) {
            setIsLoading(true);
        } else {
            setIsRefetching(true);
        }

        const url = UrlServicios('apps/');
        url.searchParams.set(
            'start',
            `${pagination.pageIndex * pagination.pageSize}`,
        );
        url.searchParams.set('size', `${pagination.pageSize}`);
        url.searchParams.set('filters', JSON.stringify(columnFilters ?? []));
        url.searchParams.set('globalFilter', globalFilter ?? '');
        url.searchParams.set('sorting', JSON.stringify(sorting ?? []));
        try {
            const response = await fetch(url.href, { method: 'GET' }, { headers: { 'X-RapidAPI-Key': sessionStorage.getItem('access_token') } });
            const json = await response.json();
            if (json) {
                setData(json);
                setRowCount(json.length);
            }
        } catch (error) {
            setIsError(true);
            console.error(error);
            return null;
        }
        setIsError(false);
        setIsLoading(false);
        setIsRefetching(false);
        return null;
    };

    useEffect(() => {
        fetchData();
    }, []);




    const columns = useMemo(
        () => [
            {
                accessorKey: 'folio_ppm', //access nested data with dot notation
                header: 'PPM',
            },
            // {
            //     accessorKey: 'appid', //access nested data with dot notation
            //     header: 'ID Hopex',
            // },
            {
                accessorKey: 'areasolicitante',
                accessorFn: (row) => `${row.areasolicitante}${row.otraarea ? "/" + row.otraarea : ''}`,
                header: 'Area',
            },
            {
                id: 'prospecto',
                accessorFn: (row) => `${row.usuariospotenciales} `,
                header: 'Prospectos'
            },
            {
                accessorFn: (row) => `RAM:${row.ram} CPU:${row.cpu} `,
                // Cell: ({ renderedCellValue, row }) => (
                //     <div>
                //         <span>RAM: {row.original.ram}</span>
                //         <br></br>
                //         <span>CPU: {row.original.cpu}</span>
                //     </div>
                // ),
                header: 'Infra'
            },
            {
                accessorFn: (row) => `U:${row.usuariosporminuto} TX:${row.numtransacciones} `,
                header: 'Uso'
            },
            {
                accessorKey: 'nombre',
                header: 'Nombre',
            },
            // {
            //     accessorKey: 'telefono',
            //     header: 'Telefono',
            // },
            // {
            //     accessorKey: 'correo', //normal accessorKey
            //     header: 'Correo',
            // },
            {
                accessorKey: 'fecharegistro',
                header: 'Fecha Registro',
                accessorFn: (row) => new Date(row.fecharegistro), //convert to Date for sorting and filtering
                id: 'startDate',
                filterFn: 'lessThanOrEqualTo',
                sortingFn: 'datetime',
                Cell: ({ cell }) => cell.getValue()?.toLocaleDateString(), //render Date as a string
                Header: ({ column }) => <em>{column.columnDef.header}</em>, //custom header markup
            },
        ],
        []);


    const csvOptions = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true,
        useBom: true,
        useKeysAsHeaders: false,
        headers: columns.map((c) => c.header),
    };
    const csvExporter = new ExportToCsv(csvOptions);
    const handleExportRows = (rows) => {
        csvExporter.generateCsv(rows.map((row) => row.original));
    };
    const handleExportData = () => {
        csvExporter.generateCsv(data);
    };

    return (
        <div className="">
            <MaterialReactTable
                columns={columns}
                data={data}
                // enableRowSelection
                positionToolbarAlertBanner="bottom"
                renderTopToolbarCustomActions={({ table }) => (
                    <Box
                        sx={{ display: 'flex', gap: '1rem', p: '0.5rem', flexWrap: 'wrap' }}
                    >
                        <button
                            color="complementary"
                            //export all data that is currently in the table (ignore pagination, sorting, filtering, etc.)
                            onClick={handleExportData}
                            startIcon={<FileDownloadIcon />}
                            variant="contained"
                        >
                            Exportar Datos
                        </button>
                        {/* <button
                            disabled={table.getPrePaginationRowModel().rows.length === 0}
                            //export all rows, including from the next page, (still respects filtering and sorting)
                            onClick={() =>
                                handleExportRows(table.getPrePaginationRowModel().rows)
                            }
                            startIcon={<FileDownloadIcon />}
                            variant="contained"
                        >
                            Export All Rows
                        </button> */}
                        {/* <button
                            disabled={table.getRowModel().rows.length === 0}
                            //export all rows as seen on the screen (respects pagination, sorting, filtering, etc.)
                            onClick={() => handleExportRows(table.getRowModel().rows)}
                            startIcon={<FileDownloadIcon />}
                            variant="contained"
                        >
                            Export Page Rows
                        </button> */}
                        {/* <button
                            disabled={
                                !table.getIsSomeRowsSelected() && !table.getIsAllRowsSelected()
                            }
                            //only export selected rows
                            onClick={() => handleExportRows(table.getSelectedRowModel().rows)}
                            startIcon={<FileDownloadIcon />}
                            variant="contained"
                        >
                            Export Selected Rows
                        </button> */}
                    </Box>
                )}
                getRowId={(row) => row.id}
                initialState={{ showColumnFilters: false }}
                onRowSelection={
                    ({ e }) => {
                        console.log("Selección de Fila" + e);
                    }
                }
                manualFiltering
                manualPagination
                manualSorting
                editingMode="modal" //default
                enableColumnOrdering
                enableEditing
                // onEditingRowSave={handleSaveRowEdits}
                // onEditingRowCancel={handleCancelRowEdits}
                // Generar panel al dar clic al boton
                renderRowActions={
                    ( row ) => (
                        sessionStorage.getItem("rol") !== "2" ? '' : 'subheader'
                    ) ? (
                        <Box sx={{ display: 'flex', gap: '1rem' }}>
                            <Tooltip arrow placement="right" title="Editar">
                                <IconButton onClick={() => console.log(row)}>
                                    <Edit />
                                </IconButton>
                            </Tooltip>
                            <Tooltip arrow placement="right" title="Borrar">
                                <IconButton color="error" onClick={() =>
                                    window.jQuery('#modal-' + row.id).modal('open')

                                }>
                                    <Delete />
                                </IconButton>
                            </Tooltip>
                            <Modal
                                idModal={row.original.id}
                                icon="error"
                                title={"Eliminar Folio " + row.original.id}
                                description={"¿Quieres eliminar registro de  " + row.original.areasolicitante + "?"}
                                onOpen=""
                                onClose={
                                    () => {
                                        const url = UrlServicios('apps/delete/' + row.original.id);
                                        fetch(url, {
                                            method: 'POST',
                                            headers: { 'Content-Type': 'application/json' },
                                        }).then((response) => response.json())
                                            .then(() => {
                                                setTimeout(() => {
                                                    window.location.reload(true);
                                                }, 1000);
                                            })
                                            .catch((err) => {
                                                console.log(err.message);
                                            });
                                        console.log("Eliminar Folio");
                                    }
                                }
                                onSubmit={() => console.log("submit")}
                                textButton="Aceptar"
                            ></Modal>
                        </Box>
                    ) : (<></>)}
                onColumnFiltersChange={setColumnFilters}
                onGlobalFilterChange={setGlobalFilter}
                onPaginationChange={setPagination}
                onSortingChange={setSorting}
                rowCount={rowCount}
                state={{
                    columnFilters,
                    globalFilter,
                    isLoading,
                    pagination,
                    showAlertBanner: isError,
                    showProgressBars: isRefetching,
                    sorting,
                }}
                renderDetailPanel={({ row }) => (
                    <div className=''>

                        <div className="row">
                            <div className='col  s3'>
                                <h1 style={{ "margin": "0" }}>
                                    {
                                        (row.original.folio_ppm != "") ? ("F-" + row.original.folio_ppm) : (<></>)
                                    }
                                    <span>
                                        {
                                            (row.original.ev != "") ? ("EV. " + row.original.ev + "") : (<></>)
                                        }</span>

                                </h1>
                                <Chip label={(row.original.appid != "") ? ("AP" + row.original.appid) : (<></>)} color="primary" variant="outlined" />

                                <hr></hr>
                                <b>Funcionalidad: </b> {row.original.funcionalidad} <br />
                                <b>Expone Servicios: </b> {row.original.txtserviciosexpuestos}<br />
                                <b>Impactos:</b> {row.original.aplicativos}
                                <br></br>

                                <br></br>
                                <b> <i className="material-icons">wb_cloudy</i> Catálogo: </b>{row.original.catalogo}
                                {
                                    (row.original.catalogo === "OTRA") ?
                                        <span>/{row.original.otrocatalogo}</span>
                                        :
                                        <span>.</span>

                                }
                                <hr>
                                </hr>
                                <h3>Mesa Impacto</h3>
                                <hr>
                                </hr>

                                <h3>Contacto</h3>
                                <hr>
                                </hr>


                                <b>Nombre: </b> {row.original.nombre} <br />
                                <b>Telefono: </b> <a href={"tel:" + row.original.telefono}>{row.original.telefono}</a><br />
                                <b>Correo:</b> <a target={"blank"} href={"mailto:" + row.original.correo} >{row.original.correo}</a>
                            </div>
                            <div className='col s8 risk'>
                                <h3>Evaluación de Riesgos</h3>
                                {
                                    row.original.respuestas.map((e, i) => {

                                        return <p key={"response-" + i}>
                                            <span>
                                                {
                                                    (e.valor) ?
                                                        <i className="material-icons">check_box</i>
                                                        :
                                                        <i className="material-icons">check_box_outline_blank</i>
                                                }</span>
                                            {e.descripcion}
                                        </p>
                                    })
                                }
                                <hr></hr>

                                {
                                    (row.original.file_type) ? (<div>

                                        <button type="reset" style={{ float: 'left' }} className="btn" onClick={
                                            () => {
                                                window.jQuery('#modal-diagrama').modal('open');
                                            }}>Arquitectura</button>

                                        <Modal
                                            idModal="diagrama"
                                            icon="warning"
                                            title={"Diagrama" + row.original.file_name}
                                            description={
                                                (row.original.file_type && row.original.file_type.indexOf("pdf") >= 1) ? (
                                                    <iframe data={"data:application/pdf;," + row.original.diagrama}></iframe>
                                                ) : (
                                                    <img style={{ "width": "100%" }} src={row.original.file_type + "," + row.original.diagrama} alt="diagrama"></img>
                                                )
                                            }
                                            open={() => { }}
                                            textButton="Aceptar"
                                            onSubmit={() => console.log("submit")}
                                            onClose={() => { }}
                                        >
                                        </Modal>
                                    </div>) : (<></>)
                                }
                            </div>
                        </div>
                    </div>
                )}
            ></MaterialReactTable>
        </div>
    )
}
