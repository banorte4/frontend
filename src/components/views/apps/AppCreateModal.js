import { useEffect, useState, useRef } from 'react';
import { redirect } from "react-router-dom";
import { LoadingContext, UrlServicios } from '../../ContextLoading';
import { Editor } from 'react-draft-wysiwyg';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { EditorState, convertToRaw, ContentState } from 'draft-js';
// import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

export const AppCreateModal = () => {
    const formRef = useRef();

    const [tienesAppId, cambiarTiensAppId] = useState(true);
    const [tieneExpuestaApp, cambiarSerivciosExpuestos] = useState(false);
    const [tienesArea, cambiarArea] = useState('');
    const [tienesProveedor, cambiarTienesProveedor] = useState([]);
    const [apiResponseQuestions, cambiarListadoPreguntas] = useState([]);

    useEffect(() => {
        loadQuestions();
    }, [formRef]);

    useEffect(() => {
        window.jQuery('.sidenav').sidenav();
    });

    const handleWordCount = (event) => {
        const charCount = event.target.value.length;
        const maxChar = Number(event.target.attributes.maxlength.value);
        const charLength = maxChar - charCount;
        if (charLength === 0)
            alert("Caracteres sobrepasados" + maxChar);
        // this.setState({ chars_left: charLength });
    }

    const loadQuestions = () => {
        console.log(process.env);
        const url = UrlServicios('questions');
        fetch(url, { headers: { 'Content-Type': 'application/json' } })
            .then((response) => response.json())
            .then((actualData) => {
                cambiarListadoPreguntas(actualData);
            })
            .catch((err) => {
                console.log(err.message);
            });
    }
    function crearApp(data){
        const url = UrlServicios('apps');
        fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        }).then((response) => response.json()).then((responseJson) => {
            window.jQuery('#create-app').modal('close');
            window.M.toast({ html: 'Operación exitosa!' });
            window.jQuery('#modal-appcreate-success').modal('open');
            setTimeout(() => {
                window.location.reload(true);
            }, 1000)
            return redirect("/projects");
        }).catch(() => {
            window.M.toast({ html: 'ERROR EN SERVICIO!' });
            window.jQuery('#modal-appcreate-error').modal('open');
        });
    }
    function handleSubmit(evt) {
        const formData = new FormData(formRef.current);
        const data = Object.fromEntries(formData);
        const fomulario = window.jQuery('#addApp')[0];
        fomulario.reportValidity();
        if( fomulario.checkValidity()) {
            evt.preventDefault();
            let riesgosAceptados = [];
            formData.getAll("respuestas").map((e, i) => {
                return riesgosAceptados.push(e);
            })
            let respuestas = [];
            apiResponseQuestions.map((e, i) => {
                let r = {
                    valor: false,
                    descripcion: e.descripcion,
                    pregunta: {
                        id: e.id
                    }
                };
                if (riesgosAceptados.indexOf(e.id.toString()) >= 0) {
                    r.valor = true;
                } else {
                    r.valor = false;
                }
                return respuestas.push(r);
            });
            console.log(data);
            data.respuestas = respuestas;
            //Usuario de session
            data.usuario_alta = {
                id: 1
            }

            var fileInput = document.getElementById('myFile');

            var file = fileInput.files[0];
            var reader = new FileReader();

            reader.onload = function (event) {
                var base64Data = event.target.result.split(',')[1]; // Remove data:image/*;base64

                //Imagen en base de datos, para renderearla concatenar con una ,
                data.file_type = event.target.result.split(',')[0];
                data.file_name = file.name;
                
                data.diagrama = base64Data

                crearApp(data);
            };
            if( typeof file !== "undefined"){                
                reader.readAsDataURL(file);
            }
            else{
                crearApp(data);
            }

        } else {
            window.M.toast({ html: 'Formulario no completado!' });
            window.jQuery('#modal-appcreate-warning').modal('open');
            evt.preventDefault();
            return false;
        }
    }
    return (
        <div className="app-create-component">
            <div className="modal create-app" id="create-app">
                <div className="icon-close modal-close">
                    <img src="../img/icono_cerrar.svg" alt="icon" />
                </div>
                <div className="center">
                    <h3>Registrar Aplicación</h3>
                </div>
                <div className="center" >
                    <form id="addApp" noValidate onSubmit={handleSubmit} ref={formRef} style={{ "padding": 10 }} >
                        <ul className="collapsible">
                            <li className="active">
                                <div className="collapsible-header">
                                    <i className="material-icons">verified_user</i>
                                    Solicitante
                                    <span className="new badge" hidden data-badge-caption="pendiente">
                                    </span>
                                </div>
                                <div className="collapsible-body">

                                <div className="row " >
                                            <div className="col s6">
                                                <div className="input-field">
                                                    <input
                                                        required="true"
                                                        minLength="1" maxLength="6"
                                                        type="text"
                                                        // pattern="AP[0-9]{1,10}"
                                                        pattern="[0-9]{1,7}"
                                                        // placeholder="AP0123" 
                                                        size="10" id="folio_ppm"
                                                        className="validate"
                                                        name="folio_ppm" />
                                                    <label className="active" htmlFor="folio_ppm"> Folio PPM </label>
                                                    <span className="helper-text" data-error="" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="col s6">
                                                <div className="input-field">
                                                    <input
                                                        required="true"
                                                        // pattern="AP[0-9]{1,10}"
                                                        pattern="[0-9]{1,3}"
                                                        defaultValue={1}
                                                        placeholder="001" 
                                                        size="3" id="ev" minLength="1" maxLength={3} type="text"
                                                        className="validate"
                                                        name="ev" />
                                                    <label className="active" htmlFor="ev"> EV </label>
                                                    <span className="helper-text" data-error="" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>

                                    <div className="center data-cliente">
                                        <div className="row">
                                            <div className="col s12">
                                                <div className="center"> ¿ Tienes Id Hopex ?</div>
                                                <div className="center" >
                                                    <div className="switch">
                                                        <label>
                                                            No
                                                            <input type="checkbox" name="tieneAppId" id="tieneAppId" defaultChecked
                                                                onChange={(event) => {
                                                                    cambiarTiensAppId(event.target.checked);
                                                                }}
                                                                defaultValue
                                                            />
                                                            <span className="lever"></span>
                                                            Si
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row " id="appiddiv"
                                            hidden={!tienesAppId}>
                                            <div className="col s12">
                                                <div className="input-field">
                                                    <input
                                                        disabled={!tienesAppId}
                                                        required={tienesAppId}
                                                        minLength="1" maxLength="4"
                                                        type="text"
                                                        // pattern="AP[0-9]{1,10}"
                                                        pattern="[0-9]{1,4}"
                                                        placeholder="0123" 
                                                        size="10" id="appid"
                                                        className="validate"
                                                        name="appid" />
                                                    <label className="active" htmlFor="appid"> App ID Hopex</label>
                                                    <span className="helper-text" data-error="Ingresa los 4 dígitos" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <div className="input-field">
                                                    <input minLength="3" maxLength="255" type="text"
                                                        pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}"
                                                        size="60" title="Registra un nombre válido" required id="nombre_completo"
                                                        className="validate" name="nombre" defaultValue={sessionStorage.getItem("fullname")} />
                                                    <label className="active" htmlFor="nombre_completo">Nombre Solicitante </label>
                                                    <span className="helper-text" data-error="Ingresa el nombre completo" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s6">
                                                <div className="input-field">
                                                    <input minLength="10" maxLength="10" type="text" title="Numero de teléfono a 10 dígitos"
                                                        pattern="[0-9]{10}"
                                                        required className="validate" name="telefono" />
                                                    <label className="active" htmlFor="telefono">Teléfono</label>
                                                    <span className="helper-text" data-error="Agregá un número de teléfono de 10 dígitos" data-success=""></span>
                                                </div>
                                            </div>

                                            <div className="col s6">
                                                <div className="input-field s12">
                                                    <input minLength="10" maxLength="250" type="email" title="Correo electrónico" size="200" required className="validate" id="correo" name="correo" />
                                                    <label className="active" htmlFor="correo" > Correo electrónico </label>
                                                    <span className="helper-text" data-error="Correo inválido" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className='col s12'>
                                                <div className="input-field s12">
                                                    <select id="areasolicitante" required title="Aréa Solicitante" name="areasolicitante"
                                                        onChange={(event) => {
                                                            cambiarArea(event.target.value);
                                                        }}>
                                                        <option defaultValue="Arquitectura Empresarial">Arquitectura Empresarial</option>
                                                        <option defaultValue="Desarrollo Banca Mayorista y Mercados">Desarrollo Banca Mayorista y Mercados</option>
                                                        <option defaultValue="Desarrollo y Servicios">Desarrollo y Servicios</option>
                                                        <option defaultValue="Desarrollo y Pruebas">Desarrollo y Pruebas</option>
                                                        <option defaultValue="Infraestructura">Infraestructura</option>
                                                        <option defaultValue="Oficina de Logística">Oficina de Logística</option>
                                                        <option defaultValue="Seguridad">Seguridad</option>
                                                        <option defaultValue="Otro">Otro</option>

                                                    </select>
                                                    <label className="active" htmlFor="areasolicitante">Aréa Solicitante</label>
                                                    <span className="helper-text" data-error="Seleccióna tu aréa" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row" hidden={!(tienesArea === "Otro")}>
                                            <div className='col s12 '>
                                                <div className="input-field" >
                                                    <input
                                                        disabled={!(tienesArea === "Otro")}
                                                        required={(tienesArea === "Otro")}
                                                        minLength="3" maxLength="255" type="text" title="Nombre de otra aréa"
                                                        size="200" className="validate" id="otraarea" name="otraarea" defaultValue="" />
                                                    <label className="active" htmlFor="otraarea"> Otra Área </label>
                                                    <span className="helper-text" data-error="Describe tu área" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="material-icons">warning</i>Calculadora de Riesgos</div>
                                <div className="collapsible-body">
                                    <div className="center calculadora-riesgo">
                                        {
                                            apiResponseQuestions.map((element, index) => {
                                                return <div className="row" key={"preg" + index}>
                                                    <div className="question">
                                                        <div className="center"> {element.descripcion} </div>
                                                        <div className="switchdiv">
                                                            <div className="switch">
                                                                <label>
                                                                    <input type="checkbox" name={"respuestas"} value={
                                                                        element.id
                                                                    } />
                                                                    <span className="lever"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="material-icons">timeline</i>Capacidad</div>
                                <div className="collapsible-body">
                                    <div className="center" id="capacidad">
                                        <div className="row ">
                                            <div className="col s6 ">
                                                <div className="input-field s12">
                                                    <input minLength="1" maxLength="8" type="number" title="RAM" id="ram" pattern="[0-9]{1,8}"
                                                        name="ram" size="10" className="validate" defaultValue="250"
                                                    // oninput="numberChange(this)" 
                                                    />
                                                    <label htmlFor="ram">RAM (MiB)</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className='col s6'>
                                                <div className="input-field s12">
                                                    <input minLength="1" maxLength="8" type="number" title="CPU" id="cpu" pattern="[0-9]{1,8}"
                                                        name="cpu" size="10" className="validate" defaultValue="100"
                                                    />
                                                    <label htmlFor="CPU">CPU (milicores)</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row ">
                                            <div className='col s6'>
                                                <div className="input-field s12">
                                                    <input minLength="1" maxLength="8" type="number" title="Usuarios" id="usuariosporminuto"
                                                        pattern="[0-9]{1,8}" size="10" className="validate" name="usuariosporminuto" defaultValue="100"
                                                    />
                                                    <label htmlFor="usuariosporminuto">Usuarios por minuto</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="col s6">
                                                <div className="input-field s12">
                                                    <input minLength="1" maxLength="8" type="number" title="Transacciones por minuto" id="numtransacciones"
                                                        pattern="[0-9]{1,8}" name="numtransacciones" size="10" className="validate" defaultValue="10000"
                                                    // oninput="numberChange(this)" 
                                                    />
                                                    <label htmlFor="inputNumFide">Transacciones por minuto</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row ">
                                            <div className='col s12'>
                                                <div className="input-field s12">
                                                    <input minLength="1" maxLength="8" type="number" title="Usuarios" id="usuariospotenciales"
                                                        pattern="[0-9]{1,8}" name="usuariospotenciales" size="10" className="validate" defaultValue="1000"
                                                    // oninput="numberChange(this)" 
                                                    />
                                                    <label htmlFor="usuariospotenciales">Usuarios Potenciales</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div></div>
                                        </div>

                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="material-icons">wb_cloudy</i>Servicios Externos</div>
                                <div className="collapsible-body">
                                    <div className="center" id="capacidad">

                                        <div className="row">
                                            <div className="s12">
                                                <div className="center"> ¿Tienes servicios expuestos al exterior? </div>
                                                <div className="center" >
                                                    <div className="switch">
                                                        <label>
                                                            No
                                                            <input type="checkbox" name="serviciosexpuestos" 
                                                                onChange={(event) => {
                                                                    cambiarSerivciosExpuestos(event.target.checked);
                                                                }} />
                                                            <span className="lever"></span>
                                                            Si
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row"  hidden={!tieneExpuestaApp}>
                                            <div className='s12'>
                                                Nombra los servicios expuestos
                                            </div>
                                            <div className="input-text-area-field s12">
                                                <textarea className="form-control" id="serviciosexpuestos" maxLength="250" onChange={handleWordCount} required
                                                defaultValue="La aplicación es de uso interno"
                                                name="txtserviciosexpuestos" placeholder="Descripción de los servicios a exponer" onPaste={(e) => { e.preventDefault();return false; } } ></textarea>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="input-field s12">
                                                <select id="catalogo" required title="Tienes proveedor de nube actual" name="catalogo"
                                                    onChange={(event) => {
                                                        console.log("nube");
                                                        cambiarTienesProveedor(event.target.value);
                                                    }}>
                                                    <option defaultValue="OnPremise">OnPremise</option>
                                                    <option defaultValue="AWS">AWS</option>
                                                    <option defaultValue="AZURE">AZURE</option>
                                                    <option defaultValue="GCP">GCP</option>
                                                    <option defaultValue="OTRA">OTRA</option>
                                                </select>
                                                <label className="active" htmlFor="catalogo">¿Tienes proveedor de nube actual?</label>
                                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                            </div>
                                        </div>
                                        <div className="row" hidden={!(tienesProveedor === "OTRA")}>
                                            <div className="input-field s12">
                                                <input
                                                    disabled={!(tienesProveedor === "OTRA")}
                                                    required={(tienesProveedor === "OTRA")}
                                                    minLength="3" maxLength="255" type="text" title="Nombre del otro proveedor"
                                                    name="otrocatalogo" size="200" className="validate" id="otrocatalogo"
                                                />
                                                <label className="active" htmlFor="otrocatalogo"> Otro proveedor de nube </label>
                                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="s12">
                                                <div className="center"> Adjunta el diagrama de solución (Imagen)</div>
                                                <input type="file" accept=".jpg,.png" id="myFile" name="filename" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="collapsible-header"><i className="material-icons">wb_sunny</i>Funcionalidad</div>
                                <div className="collapsible-body">
                                    <div className="row">
                                        <div className="s12">
                                            <h3>Resumen de funcionalidad de negocio </h3>
                                            <div className="input-text-area-field s12">
                                                <textarea name="funcionalidad" id="funcionalidad" maxLength="500" onChange={handleWordCount} required
                                                    placeholder="Funcionalidad de negocio esperada" 
                                                    onPaste={(e) => { e.preventDefault(); return false; } } defaultValue="Ejemplo: Aplicación de galería de imágenes en la que los usuarios pueden cargar imágenes, verlas en una lista y descargarlas etc..."></textarea>
                                            </div>
                                        </div>
                                        <div className="s12">
                                            <h3>Nombra los aplicativos impactados</h3>
                                            <div className="input-tex-area-field s12">
                                                <textarea name="aplicativos" id="aplicativos" maxLength="500" onChange={handleWordCount} required
                                                    placeholder="Aplicativos Impactados" 
                                                    onPaste={(e) => { e.preventDefault(); return false; } } defaultValue="Ejemplo: MCA, Altamira, ASM, Base24... "></textarea>

                                            </div>


                                            {/* <Editor
                                                wrapperClassName="demo-wrapper"
                                                editorClassName="demo-editor"
                                                onChange={(e)=>{

                                                    console.log(e.blocks[0].text)
                                                }}
                                            /> */}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li hidden={true}>
                                <div className="collapsible-header"><i className="material-icons">supervisor_account</i>Mesa de Impactos</div>
                                <div className="collapsible-body">
                                    {/* <!-- mesa imapacto--> */}
                                    <div className="center" id="mesa-imapactos" >
                                        <div className="">
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactoarquitectotecnico" id="mesaimpactoarquitectotecnico" required
                                                        title="Arquitecto Técnico" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactoarquitectotecnico">¿Tienes Arquitecto Técnico?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactoge2" id="mesaimpactoge2" required title="GE2" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactoge2">¿Tienes GE2?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactoliderqa" id="mesaimpactoliderqa" required title="Lider QA" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactoliderqa">¿Tienes Lider de QA?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactoconsultorseguridad" id="mesaimpactoconsultorseguridad" required
                                                        title="Consultor Seguridad" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactoconsultorseguridad">¿Tienes Consultor de Seguridad?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactoarquitectoseguridad" id="mesaimpactoarquitectoseguridad" required
                                                        title="Arquitecto Seguridad" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactoarquitectoseguridad">¿Tienes arquitecto de seguridad?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactogv" id="mesaimpactogv" required title="Gestion Versiones" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                        <option defaultValue="Sinai Ochoa Cortez">Sinai Ochoa Cortez</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactogv">¿Tienes gestor de versiones y ambientes previos?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select name="mesaimpactoarquitectoempresarial" id="mesaimpactoarquitectoempresarial"
                                                        required title="Arquitecto empresarial" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactoarquitectoempresarial">¿Tienes arquitecto de
                                                        empresarial?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select id="mesaimpactocomponentesreutilizables" required title="Componentes reutilizables" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactocomponentesreutilizables">¿Tienes componentes
                                                        reutilzables?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="input-field s12">
                                                    <select id="mesaimpactocomponentesciso" required title="Componentes en la nube (CISO)" disabled>
                                                        <option defaultValue="" disabled >en proceso de asignación</option>
                                                    </select>
                                                    <label className="active" htmlFor="mesaimpactocomponentesciso">¿Tienes componentes en la nube CISO?</label>
                                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- aviso privacidad --> */}
                                    <div className="center" hidden>
                                        <label>
                                            <input type="checkbox" id="aviso" defaultValue="true" className="filled-in" />
                                            <span className="subtitulo04" >He leído y acepto el
                                                <a href="https://www.banorte.com/wps/portal/gfb/Home/banorte-te-informa/aviso_de_privacidad/"
                                                    rel="noreferrer" target={"_blank"} style={{ "color": "#eb0029", "textdecoration": "underline" }}>Aviso de Privacidad</a>
                                            </span>
                                        </label>
                                        <span className="helper-text" data-error="Favor de acpetar el aviso de privacidad" data-success=""></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <br></br>
                        <button type="submit" style={{ 'width': '120px' }} className="btn" >Registrar</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
