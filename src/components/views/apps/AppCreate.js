import React from 'react';
import { redirect } from "react-router-dom";
import { LoadingContext, UrlServicios } from '../../ContextLoading';


export const AppCreate = () => {
    const formRef = React.useRef();
    function handleSubmit(evt) {
        if (window.jQuery('#addApp')[0].checkValidity()) {
            evt.preventDefault();
            const formData = new FormData(formRef.current);
            const data = Object.fromEntries(formData);
            console.log(data);

            const url = UrlServicios('apps');
            fetch(url, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            }).then((response) => response.json()).then((responseJson) => {
                window.jQuery('#create-app').modal('close');
                window.M.toast({ html: 'Operación exitosa!' });
                window.jQuery('#modal-appcreate-success').modal('open');
                return redirect("/projects");
            }).catch(() => {
                window.M.toast({ html: 'ERROR EN SERVICIO!' });
                window.jQuery('#modal-appcreate-error').modal('open');
            });
        } else {
            window.M.toast({ html: 'Formulario no completado!' });
            window.jQuery('#modal-appcreate-warning').modal('open');
            evt.preventDefault();
            return false;
        }
    }
    
    const [tienesAppId, setTienesAppId] = React.useState(true);
    const [tienesArea, setTienesArea] = React.useState('');
    const [tienesProveedor, setTienesProveedor] = React.useState('');

    return (
        <div className="app-create-component">
            <div className="container" >

                <form id="addApp" noValidate onSubmit={handleSubmit} ref={formRef} style={{ "padding": 10 }} >

                    <h3>Registrar Aplicación</h3>
                    <div className="center data-cliente">
                        <div className="row">
                            <div className="col s12">
                                <div className="center"> ¿ Tienes Id Hopex ?</div>
                                <div className="center" >
                                    <div className="switch">
                                        <label>
                                            No
                                            <input type="checkbox" name="tieneAppId" id="tieneAppId" defaultChecked
                                                onChange={(event) => {
                                                    setTienesAppId(event.target.checked);
                                                }}
                                                defaultValue
                                            />
                                            <span className="lever"></span>
                                            Si
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="row " id="appiddiv"
                                hidden={!tienesAppId}>
                                <div className="col s12">
                                    <div className="input-field">
                                        <input
                                            disabled={!tienesAppId}
                                            required={tienesAppId}
                                            minLength="1" maxLength="6"
                                            type="text"
                                            pattern="AP[0-9]{1,10}"
                                            // placeholder="AP0123" 
                                            size="10" id="appid"
                                            className="validate"
                                            name="appid" />
                                        <label className="active" htmlFor="appid"> ID Hopex (AP0123)</label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s12">
                                    <div className="input-field">
                                        <input minLength="3" maxLength="255" type="text"
                                            pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,48}"
                                            size="60" title="Registra un nombre válido" required id="nombre_completo"
                                            className="validate" name="nombre" />
                                        <label className="active" htmlFor="nombre_completo">Nombre Solicitante </label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s6">
                                    <div className="input-field">
                                        <input minLength="10" maxLength="10" type="text" title="Numero de teléfono a 10 dígitos"
                                            pattern="[0-9]{10}"
                                            required className="validate" name="telefono" />
                                        <label className="active" htmlFor="telefono">Teléfono</label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>

                                <div className="col s6">
                                    <div className="input-field s12">
                                        <input minLength="10" maxLength="250" type="email" title="Correo electrónico" size="200" required className="validate" id="correo" name="correo" />
                                        <label className="active" htmlFor="correo" > Correo electrónico </label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className='col s12'>
                                    <div className="input-field s12">
                                        <select id="areasolicitante" required title="Aréa Solicitante" name="areasolicitante"
                                            onChange={(event) => {
                                                setTienesArea(event.target.value);
                                            }}>
                                            <option defaultValue="Arquitectura Empresarial">Arquitectura Empresarial</option>
                                            <option defaultValue="Desarrollo Banca Mayorista y Mercados">Desarrollo Banca Mayorista y Mercados</option>
                                            <option defaultValue="Desarrollo y Servicios">Desarrollo y Servicios</option>
                                            <option defaultValue="Desarrollo y Pruebas">Desarrollo y Pruebas</option>
                                            <option defaultValue="Infraestructura">Infraestructura</option>
                                            <option defaultValue="Oficina de Logística">Oficina de Logística</option>
                                            <option defaultValue="Seguridad">Seguridad</option>
                                            <option defaultValue="Otro">Otro</option>

                                        </select>
                                        <label className="active" htmlFor="areasolicitante">Aréa Solicitante</label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>
                            </div>
                            <div className="row" hidden={!(tienesArea === "Otro")}>
                                <div className='col s12 '>
                                    <div className="input-field" >
                                        <input
                                            disabled={!(tienesArea === "Otro")}
                                            required={(tienesArea === "Otro")}
                                            minLength="3" maxLength="255" type="text" title="Nombre de otra aréa"
                                            size="200" className="validate" id="otraarea" name="otraarea" defaultValue="" />
                                        <label className="active" htmlFor="otraarea"> Otra Área </label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="center" id="capacidad">
                        <div className="row ">
                            <div className="col s6 ">
                                <div className="input-field s12">
                                    <input minLength="1" maxLength="8" type="number" title="RAM" id="ram" pattern="[0-9]{1,8}"
                                        name="ram" size="10" className="validate" defaultValue="250"
                                    // oninput="numberChange(this)" 
                                    />
                                    <label htmlFor="ram">RAM (MiB)</label>
                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                </div>
                            </div>
                            <div className='col s6'>
                                <div className="input-field s12">
                                    <input minLength="1" maxLength="8" type="number" title="CPU" id="cpu" pattern="[0-9]{1,8}"
                                        name="cpu" size="10" className="validate" defaultValue="100"
                                    />
                                    <label htmlFor="CPU">CPU (milicores)</label>
                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                </div>
                            </div>
                        </div>
                        <div className="row ">
                            <div className='col s6'>
                                <div className="input-field s12">
                                    <input minLength="1" maxLength="8" type="number" title="Usuarios" id="usuariosporminuto"
                                        pattern="[0-9]{1,8}" size="10" className="validate" name="usuariosporminuto" defaultValue="100"
                                    />
                                    <label htmlFor="usuariosporminuto">Usuarios por minuto</label>
                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                </div>
                            </div>
                            <div className="col s6">
                                <div className="input-field s12">
                                    <input minLength="1" maxLength="8" type="number" title="Transacciones por minuto" id="numtransacciones"
                                        pattern="[0-9]{1,8}" name="numtransacciones" size="10" className="validate" defaultValue="10000"
                                    // oninput="numberChange(this)" 
                                    />
                                    <label htmlFor="inputNumFide">Transacciones por minuto</label>
                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                </div>
                            </div>
                        </div>
                        <div className="row ">
                            <div className='col s12'>
                                <div className="input-field s12">
                                    <input minLength="1" maxLength="8" type="number" title="Usuarios" id="usuariospotenciales"
                                        pattern="[0-9]{1,8}" name="usuariospotenciales" size="10" className="validate" defaultValue="1000"
                                    // oninput="numberChange(this)" 
                                    />
                                    <label htmlFor="usuariospotenciales">Usuarios Potenciales</label>
                                    <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                </div></div>
                        </div>

                    </div>

                    <div className="center" id="expuesto">
                        <div className="row">
                            <div className="s12">
                                <div className="center"> ¿Tienes servicios expuestos al exterior? </div>
                                <div className="center" >
                                    <div className="switch">
                                        <label>
                                            No
                                            <input type="checkbox" name="serviciosexpuestos" />
                                            <span className="lever"></span>
                                            Si
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" ng-hide="form.serviciosexpuestos=='N'">
                            <div className='s12'>
                                Describe los servicios
                            </div>
                            <div className="input-text-area-field s12">
                                <textarea className="form-control" id="serviciosexpuestos"
                                    name="txtserviciosexpuestos" placeholder="Descripción de los servicios a exponer"></textarea>
                            </div>
                        </div>

                        <div className="row">
                            <div className="input-field s12">
                                <select id="catalogo" required title="Tienes proveedor de nube actual" name="catalogo"
                                    onChange={(event) => {
                                        console.log("nube");
                                        setTienesProveedor(event.target.value);
                                    }}>
                                    <option defaultValue="N/A">N/A</option>
                                    <option defaultValue="AWS">AWS</option>
                                    <option defaultValue="AZURE">AZURE</option>
                                    <option defaultValue="GCP">GCP</option>
                                    <option defaultValue="OTRA">OTRA</option>
                                </select>
                                <label className="active" htmlFor="catalogo">¿Tienes proveedor de nube actual?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row" hidden={!(tienesProveedor === "OTRA")}>
                            <div className="input-field s12">
                                <input
                                    disabled={!(tienesProveedor === "OTRA")}
                                    required={(tienesProveedor === "OTRA")}
                                    minLength="3" maxLength="255" type="text" title="Nombre del otro proveedor"
                                    name="otrocatalogo" size="200" className="validate" id="otrocatalogo"
                                />
                                <label className="active" htmlFor="otrocatalogo"> Otro proveedor de nube </label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="s12">
                                <div className="center"> Adjunta el diagrama de solución </div>
                                <input type="file" id="myFile" name="filename" />
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="s12">
                            <div>Funcionalidad de negocio </div>
                            <div className="input-text-area-field s12">
                                <textarea name="funcionalidad" id="funcionalidad" required
                                    placeholder="Funcionalidad de negocio esperada"></textarea>
                            </div>
                        </div>
                        <div className="s12">
                            <div>Aplicativos Impactados</div>
                            <div className="input-tex-area-field s12">
                                <textarea name="aplicativos" id="aplicativos" required
                                    placeholder="Aplicativos Impactados" defaultValue="" ></textarea>
                            </div>
                        </div>
                    </div>

                    <div className="center" id="mesa-imapactos" >
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactoarquitectotecnico" id="mesaimpactoarquitectotecnico" required
                                    title="Arquitecto Técnico" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactoarquitectotecnico">¿Tienes Arquitecto Técnico?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactoge2" id="mesaimpactoge2" required title="GE2" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactoge2">¿Tienes GE2?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactoliderqa" id="mesaimpactoliderqa" required title="Lider QA" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactoliderqa">¿Tienes Lider de QA?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactoconsultorseguridad" id="mesaimpactoconsultorseguridad" required
                                    title="Consultor Seguridad" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactoconsultorseguridad">¿Tienes Consultor de Seguridad?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactoarquitectoseguridad" id="mesaimpactoarquitectoseguridad" required
                                    title="Arquitecto Seguridad" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactoarquitectoseguridad">¿Tienes arquitecto de seguridad?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactogv" id="mesaimpactogv" required title="Gestion Versiones" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                    <option defaultValue="Sinai Ochoa Cortez">Sinai Ochoa Cortez</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactogv">¿Tienes gestor de versiones y ambientes previos?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select name="mesaimpactoarquitectoempresarial" id="mesaimpactoarquitectoempresarial"
                                    required title="Arquitecto empresarial" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactoarquitectoempresarial">¿Tienes arquitecto de
                                    empresarial?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select id="mesaimpactocomponentesreutilizables" required title="Componentes reutilizables" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactocomponentesreutilizables">¿Tienes componentes
                                    reutilzables?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field s12">
                                <select id="mesaimpactocomponentesciso" required title="Componentes en la nube (CISO)" disabled>
                                    <option defaultValue="" disabled >en proceso de asignación</option>
                                </select>
                                <label className="active" htmlFor="mesaimpactocomponentesciso">¿Tienes componentes en la nube CISO?</label>
                                <span className="helper-text" data-error="Completar campo" data-success=""></span>
                            </div>
                        </div>
                    </div>
                    <br></br>
                    <button type="submit" style={{ 'width': '120px' }} className="btn" >Registrar</button>
                </form>
            </div>
        </div>
    )
}
