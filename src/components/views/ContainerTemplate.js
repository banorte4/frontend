import React from 'react';

export const ContainerTemplate = ({ menu, modal, container }) => {
    return (
        <div className="row">
            {
                modal
            }
            <div className="migaja" hidden>

                <nav>
                    <div className="nav-wrapper">
                        <div className="col s12">
                            {
                                window.location.pathname.split('/').map((e, i) => {
                                    return <a href="#!" key={i} className="breadcrumb">{e}</a>
                                })
                            }
                        </div>
                    </div>
                </nav>
            </div>
            <div className="col s12 m12 l12 container-central">
                <div className="menu-container">
                    <div className='row'>
                        <div className='col s4'>
                        </div>
                        <div className='col s4'>
                            <span></span>
                        </div>
                        <div className='col s4'>
                                <form action="">
                            {
                                    menu
                            }
                                </form>
                        </div>
                    </div>
                </div>
            </div>

            <div className="col s12 m12 l12 central-container" >
                {
                    container
                }
            </div>
        </div>
    )
}
