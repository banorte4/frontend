import React, { useState, createContext, useContext, useEffect } from "react";
import { Route, Routes, useNavigate } from 'react-router-dom';
import { LoadingContext, UrlServicios } from '../ContextLoading';
import './home.scss';

export const Home = (props) => {
    const [password, setPassword] = React.useState('');
    const formRef = React.useRef();


    const navigate = useNavigate();

    useEffect(() => {
        console.log("Inicio de home");
        console.log(props);
        if (props.token && props.token.length >= 10) {
            navigate('/projects', { replace: true });
        }
        window.jQuery('.carousel').carousel({
            fullWidth: true,
            indicators: true,
            duration: 100,
            noWrap: true,
            numVisible: 1,
            padding: 0
        });
    }, []);

    const { loading, setLoading } = useContext(LoadingContext);

    function handleSubmit(evt) {

        evt.preventDefault();
        const formData = new FormData(formRef.current);
        const data = Object.fromEntries(formData);
        const controller = new AbortController();
        setTimeout(() => controller.abort(), 10000);
        const url = UrlServicios('users/login/');
        if (data.username === "" && data.password === "") {
        } else {

            setLoading(true);
            fetch(url, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((actualData) => {
                    if (actualData.token) {
                        sessionStorage.setItem("access_token", actualData.token);
                        sessionStorage.setItem("fullname", actualData.fullname);
                        sessionStorage.setItem("rol", actualData.rol.id)
                        props.login(actualData.token);
                        console.log("Login Exitoso");
                        navigate('/projects', { replace: true });
                        setLoading(false);
                    }
                    else {
                        window.M.toast({ html: 'Servicio se consulto, pero no genero el token de acceso!' })
                    }
                })
                .catch((err) => {
                    setPassword("");
                    setLoading(false);
                    window.M.toast({ html: 'Credenciales invalidas!' });
                });
        }
    }
    return (

        <div className='row alineacionVertiacl' >
            <div className="col s12 m6 l4 ">
                <div className="colAlineacionVertiacl center-align">
                    <div className="homeLogin">
                        <img src="../../img//banorte_logo.png" alt="logo" style={{ "width": "100%" }}></img>
                        <hr></hr>
                        <h3>PLATAFORMA DEVSECOPS - modificacion 13/10/2023</h3>
                        <p>Bienvenido</p>
                        <div className="center" >
                            <form onSubmit={handleSubmit} ref={formRef}>
                                <div className="row " >
                                    <div className="input-field s12">
                                        <input minLength="1" maxLength="8" type="text" title="user"
                                            pattern="a[0-9]{1,7}" size="10"
                                            name="username"
                                            className="validate" />
                                        <label htmlFor='user'>Usuario</label>
                                        <span className="helper-text" data-error="Ejemplo del usuario: a8049951" data-success=""></span>
                                    </div>
                                </div>
                                <div className="row " >
                                    <div className="input-field s12">
                                        <input minLength="1" type="password" title="password"
                                            autoComplete="off"
                                            name="password"
                                            className="validate"
                                            defaultValue={password}
                                            onChange={(event) => {
                                                setPassword(event.target.value);
                                            }}
                                        />
                                        <label htmlFor={'pass'}>Password</label>
                                        <span className="helper-text" data-error="Completar campo" data-success=""></span>
                                    </div>
                                </div>
                                <hr></hr>
                                <button type="submit" className="btn" >ENTRAR</button>
                                <br></br>
                                <p style={{ "fontSize": "10px" }}>Grupo Financiero Banorte 2023 Todos los Derechos reservados</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col s12 m6 l8">
                <div className="colAlineacionVertiacl preview" >
                    {/* <img src="../../img/screen01.png" alt="b"></img> */}
                    <div className="carousel carousel-slider  " >
                        <div className="carousel-item transparent black-text" href="#two!">
                            <h3>Portal DevSecOps</h3>
                            <img src="../../img/screen01.png" alt="b"></img>
                        </div>
                        <div className="carousel-item transparent white-text" href="#four!">
                            <h3>Apps IaSS</h3>
                            <img src="https://www.servnet.mx/hs-fs/hubfs/Blog/Blog_Art%C3%ADculos/imagenes%20ms_art%20feb%202022/Que%20es%20Iaas/saas.png?width=900&name=saas.png" alt="cloud">
                            </img>
                        </div>
                        <div className="carousel-item transparent white-text" href="#one!">
                            <p className="white-text"></p>
                            <h3>Proceso Agiles</h3>
                            <a target="_blank" rel="noreferrer" href="https://gfbanorte.sharepoint.com/sites/DevSecOpssomosTODOS/_layouts/15/stream.aspx?id=%2Fsites%2FDevSecOpssomosTODOS%2FShared%20Documents%2FVideos%20Informativos%2FDevSecOps%2FIntroducio%CC%81n%2EMOV&ga=1">
                                <img src="../../img/devsecops-life-cycle.png" alt="devsecops"></img>
                            </a>
                        </div>
                        <div className="carousel-item transparent white-text" href="#three!">
                            <h3>Pipelines</h3>

                            <a target="_blank" rel="noreferrer" href="https://gfbanorte.sharepoint.com/sites/DevSecOpssomosTODOS/_layouts/15/stream.aspx?id=%2Fsites%2FDevSecOpssomosTODOS%2FShared%20Documents%2FVideos%20Informativos%2FSeguridad%2FContenedores%5FDAST%2DSAST%2DIMAGE%2Emp4&ga=1">
                                <img src="https://blog.callr.tech/gitlab-ansible-docker-ci-cd/gitlab-docker-ansible.png" alt="cultura"></img>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col s12">

                <ul className='bg-bubbles'>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>

    )
}
