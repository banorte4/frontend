import React, { useEffect, useMemo, useState } from 'react';
import MaterialReactTable from 'material-react-table';
import { Modal } from '../../Modal';
import { LoadingContext, UrlServicios } from '../../ContextLoading';

//Para acciones del grid
import {
    Box,
    IconButton,
    Tooltip,
} from '@mui/material';
import { Delete } from '@mui/icons-material';

export const QuestionsList = () => {
    //data and fetching state
    const [data, setData] = useState([]);
    const [isError, setIsError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isRefetching, setIsRefetching] = useState(false);
    const [rowCount, setRowCount] = useState(0);

    //table state
    const [columnFilters, setColumnFilters] = useState([]);
    const [globalFilter, setGlobalFilter] = useState('');
    const [sorting, setSorting] = useState([]);
    const [pagination, setPagination] = useState({
        pageIndex: 0,
        pageSize: 10,
    });

    const fetchData = async () => {
        if (typeof data != "undefined" && data.length) {
            setIsLoading(true);
        } else {
            setIsRefetching(true);
        }

        const url = UrlServicios('questions');
        url.searchParams.set(
            'start',
            `${pagination.pageIndex * pagination.pageSize}`,
        );
        url.searchParams.set('size', `${pagination.pageSize}`);
        url.searchParams.set('filters', JSON.stringify(columnFilters ?? []));
        url.searchParams.set('globalFilter', globalFilter ?? '');
        url.searchParams.set('sorting', JSON.stringify(sorting ?? []));
        try {
            const response = await fetch(url.href, { method: 'GET' }, { headers: { 'X-RapidAPI-Key': sessionStorage.getItem('access_token') } });
            const json = await response.json();
            if (json) {
                setData(json);
                setRowCount(json.length);
            }
        } catch (error) {
            setIsError(true);
            console.error(error);
            return;
        }
        setIsError(false);
        setIsLoading(false);
        setIsRefetching(false);
    };
    useEffect(() => {
        fetchData();
    }, [
        columnFilters,
        globalFilter,
        pagination.pageIndex,
        pagination.pageSize,
        sorting
    ]);
    const columns = useMemo(
        () => [
            {
                accessorFn: (row) => `${row.descripcion}`,
                header: 'Descripcion',
            },
            {
                accessorKey: 'fecharegistro',
                header: 'Fecha Registro',
                accessorFn: (row) => new Date(row.fecharegistro), //convert to Date for sorting and filtering
                filterFn: 'lessThanOrEqualTo',
                sortingFn: 'datetime',
                Cell: ({ cell }) => cell.getValue()?.toLocaleDateString(),
            },
        ],
        []);

    return (
        <div className="">
            <MaterialReactTable
                columns={columns}
                data={data}
                // enableRowSelection
                getRowId={(row) => row.id}
                initialState={{ showColumnFilters: false }}
                manualFiltering
                manualPagination
                manualSorting
                editingMode="modal" //default
                enableColumnOrdering
                enableEditing
                // onEditingRowSave={handleSaveRowEdits}
                // onEditingRowCancel={handleCancelRowEdits}
                renderRowActions={({ row, table }) => (
                    <Box sx={{ display: 'flex', gap: '1rem' }}>
                        <Tooltip arrow placement="right" title="Borrar">
                            <IconButton color="error" onClick={() =>
                                window.jQuery('#modal-' + row.id).modal('open')

                            }>
                                <Delete />
                            </IconButton>
                        </Tooltip>
                        <Modal
                            idModal={row.original.id}
                            icon="error"
                            title={"Eliminar Folio " + row.original.id}
                            description={"¿Quieres eliminar registro de ? : " + row.original.descripcion}
                            onOpen=""
                            onClose={
                                () => {
                                    const url = UrlServicios('questions/delete/' + row.original.id);
                                    fetch(url, {
                                        method: 'POST',
                                        headers: { 'Content-Type': 'application/json' },
                                    }).then((response) => response.json())
                                        .then(() => {
                                            setTimeout(() => {
                                                window.location.reload(true);
                                            }, 1000);
                                        })
                                        .catch((err) => {
                                            console.log(err.message);
                                        });
                                    console.log("Eliminar Folio");
                                }
                            } onSubmit={() => console.log("submit")}
                            textButton="Aceptar"
                        ></Modal>
                    </Box>
                )}
                onColumnFiltersChange={setColumnFilters}
                onGlobalFilterChange={setGlobalFilter}
                onPaginationChange={setPagination}
                onSortingChange={setSorting}
                rowCount={rowCount}
                state={{
                    columnFilters,
                    globalFilter,
                    isLoading,
                    pagination,
                    showAlertBanner: isError,
                    showProgressBars: isRefetching,
                    sorting,
                }}
            ></MaterialReactTable>
        </div>
    )
}
