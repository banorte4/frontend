import React, { useEffect } from 'react';
import './Header.css';


export const Header = (props) => {

    useEffect(() => {
        window.jQuery('.sidenav').sidenav();
    });
    return (
        <header className="Header" >
            {/* submenu */}
            <ul id="slide-out" className="sidenav ">
                <li><div className="user-view">
                    <div className="background">
                        <img src="../img/devsecops-purple-dark.jpg" alt="logo" style={{ height: '100%' }} />
                    </div>
                    <a href="user"><img className="circle" src="../img/account.png" alt="user" /></a>
                    <a href="#!name"><span className="white-text name">{sessionStorage.getItem("u")}</span></a>
                    <a href="#!email"><span className="white-text email">{sessionStorage.getItem("fullname")}</span></a>
                </div></li>
                <li><a href="projects"><i className="material-icons">apps</i>Proyectos</a></li>
                <li><div className="divider"></div></li>
                <li><a className="subheader" href="#!" ><i className="material-icons">settings</i>Administración</a></li>
                <li><a className={sessionStorage.getItem("rol") !== "11" ? '' : 'subheader'} href="users"><i className="material-icons">groups</i>Users</a></li>
                <li><a className={sessionStorage.getItem("rol") !== "11" ? '' : 'subheader'} href="questions"><i className="material-icons">help</i>Questions</a></li>
                <li><div className="divider"></div></li>
                {
                    (props.token && props.token.length > 0) ? (
                        <li>
                            <a href="#logout" onClick={props.logout} > <i className="small material-icons" >exit_to_app</i>Cerrar Sessión</a>
                        </li>
                    ) : (
                        <span></span>
                    )
                }
            </ul>
            <div className="header_top">
                <div className="layout">
                    <div className="columns2B">

                    </div>

                    <div className="columns2A">
                        <ul className="header_menu">
                            {(props.token && props.token.length > 0) ? (
                                <li>
                                    <a href="#!" data-target="slide-out" className="waves-effect waves-white sidenav-trigger show-on-large"><i className="small material-icons">menu</i></a>     </li>
                            ) : (
                                <li>
                                    {/* <a href="#login" onClick={props.login} > <i className="small material-icons" >account_circle</i></a> */}
                                </li>
                            )}
                            <li>
                                {/* <a href='/' >
                            <img src='../img/images.png'></img>
                        </a> */}
                                <a href="/" className="logo">
                                    <span></span>
                                </a>
                            </li>
                        </ul>


                    </div>
                </div>
            </div>
        </header>
    )
}
