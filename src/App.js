
import './App.css';
import React, { useState, useEffect, createContext, useContext, useMemo } from 'react';
import { Routes, Route } from "react-router-dom";

//Paginas
import { ContainerTemplate } from './components/views/ContainerTemplate';
import { Home } from './components/views/Home';

//Compomnentes
import { Login } from './components/Login';
import { Header } from './components/Header.js';
import { Modal } from './components/Modal';
import { Loading } from './components/Loading';
import { LoadingContext } from './components/ContextLoading';

//Sub Elementos
import { AppList } from './components/views/apps/AppList';
import { QuestionsList } from './components/views/questions/QuestionsList';
import { UsersList } from './components/views/users/UsersList';
import { AppCreate } from './components/views/apps/AppCreate.js';
import { AppCreateModal } from './components/views/apps/AppCreateModal.js';
function App() {

  //Generando estados  
  const [loading, setLoading] = useState(false);

  const [theme, changeTheme] = useState("");//light/dark
  const [token, login] = useState((responseJson) => {
    return sessionStorage.getItem('access_token');
  });//Setear el token de accesso

  useEffect(() => {
    document.body.className = theme;
  }, [theme]);

  const toggleTheme = () => {
    if (theme === 'light') {
      changeTheme('dark');
      window.jQuery("html").css('background', '#323E48');
    } else {
      changeTheme('light');
      window.jQuery("html").css('background', 'white');
    }
  };

  //Abrir modales
  const showModal = () => {
    window.jQuery('#create-app').modal('open');
  }
  const showLogin = () => {
    window.jQuery('#login').modal('open');
  }
  const logout = () => {
    console.log("Cerrar session");
    sessionStorage.clear();
    window.jQuery('#login').modal('close');
    login("");
    window.location="/";
  }
  return (
    <LoadingContext.Provider value={{ loading, setLoading }}>
            
      <div className={`banorte ${theme}`}>

        <Loading></Loading>
        
        <button hidden onClick={toggleTheme}>Toggle Theme</button>

      
        <Login login={login}></Login>
        {(token && token.length > 0) ? (
        <Header token={token} login={showLogin} logout={logout}></Header>
        ):
        (
          <></>
        )}
        <Routes>
          <Route path="/" element={
            <Home login={login} ></Home>
          }></Route>

          <Route path="/projects" Component={
            (sessionStorage.access_token)? () => 
            <ContainerTemplate
              modal={<AppCreateModal />}              
              menu={<button type="reset" style={{ float: 'right' }} className="btn" onClick={showModal}>Registrar Proyecto</button>}
              container={<AppList />}></ContainerTemplate>
              :() =>  <h3>DEVSECOPS : 403 ACCESS DENIED</h3>
          }></Route>

          <Route path="/create" element={
            <ContainerTemplate
              modal={null}
              menu={<button type="" style={{ float: 'right' }} className="btn" onClick={showModal}>Ver Listado</button>}
              container={<AppCreate />}></ContainerTemplate>
          }></Route>

          <Route path="/questions" element={
            <ContainerTemplate
              modal={null}
              menu={<button type="" style={{ float: 'right' }} className="btn" onClick={showModal}>Agregar Pregunta</button>}
              container={
                <QuestionsList></QuestionsList>
              }></ContainerTemplate>
          }></Route>

          <Route path="/users" element={

            <ContainerTemplate
              modal={null}
              menu={<button type="" style={{ float: 'right' }} className="btn" onClick={showModal}>Agregar Pregunta</button>}
              container={
                <UsersList></UsersList>
              }></ContainerTemplate>

          }></Route>

          <Route path='*' element={<h1>404</h1>} />

        </Routes>
        <Modal
          idModal="appcreate-success"
          icon="success"
          title="Operación Exitosa"
          description="Tus datos fueron enviados exitosamente, en breve serás contactado por el equipo DevSecOps."
          onOpen=""
          onClose={() => console.log("close")}
          onSubmit={() => console.log("submit")}
          textButton="Aceptar"
        ></Modal>
        <Modal
          idModal="appcreate-error"
          icon="error"
          title="Operación fallida"
          description="Tus datos no fueron enviados exitosamente."
          onOpen=""
          onClose={() => console.log("close")}
          onSubmit={() => console.log("submit")}
          textButton="Aceptar"
        ></Modal>
        <Modal
          idModal="appcreate-warning"
          icon="warning"
          title="Operación no completada"
          description="Todos los campos son requeridos."
          onOpen=""
          onClose={() => console.log("close")}
          onSubmit={() => console.log("submit")}
          textButton="Aceptar"
        ></Modal>
      </div>

    </LoadingContext.Provider>

  );
}

export default App;
