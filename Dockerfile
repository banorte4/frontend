FROM registry.access.redhat.com/ubi8/nginx-120:latest
COPY build .
ADD default.conf "${NGINX_CONF_PATH}"
CMD   nginx -g "daemon off;"