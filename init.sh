#!/bin/bash
npm run build 
docker build -f Dockerfile -t devsecops-react .
docker images | grep devsecops-react  
docker stop frontend  2> /dev/null && docker rm frontend  2> /dev/null
docker run -d -p 3000:8080 --name frontend devsecops-react
